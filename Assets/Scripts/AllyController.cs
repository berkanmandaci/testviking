using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllyController : MonoBehaviour
{
    [SerializeField] private Animator Animator;
    // Start is called before the first frame update
    void Start()
    {
        Animator.SetBool("Run",true);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Slerp(transform.position, transform.parent.position, 0.5f * Time.deltaTime);
        transform.rotation=Quaternion.identity;
    }
}
