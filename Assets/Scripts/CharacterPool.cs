using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CharacterPool : MonoBehaviour
{
    [SerializeField] private Transform allyPool;
    [SerializeField] private GameObject ally;
    private GateController _gateController; 
    private Process _processs;
      
     private TextMeshPro pointText;
     private int _pointValue;
     

     // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Trigger");
        if (other.CompareTag("Gate"))
        {
            _gateController = other.GetComponent<GateController>();
            _processs = _gateController._processs;
            _pointValue = _gateController.pointValue;
            
            switch (_processs)
            {
                case Process.Add:
                    
                    InstantiateCircle(_pointValue);
                    break;  
                case Process.Remove:  
                    DestroySheep(_pointValue);  
                    break;  
                case Process.Divide:  
                    InstantiateCircle(_pointValue);
                    break;  
                case Process.Multiply:  
                    InstantiateCircle(_pointValue); 
                    break;  
                default:  
                    Console.WriteLine("Value didn’t match earlier.");  
                    break;  
            }
        }
    }

    public void InstantiateCircle (int pieceCount) 
    {
        Debug.Log("instantiate");
        float angle = 360f / (float)pieceCount;
        for (int i = 0; i < pieceCount; i++)
        {
            Quaternion rotation = Quaternion.AngleAxis(i * angle, Vector3.up);
            Vector3 direction = rotation * Vector3.forward;
 
            Vector3 position = allyPool.position + (direction * 0.5f);
            Instantiate(ally, position, rotation,allyPool);
        }
    }
    public void DestroySheep(int pieceCount)
    {
        if (allyPool.childCount < pieceCount) pieceCount = allyPool.childCount;
          
        for (int i = 0; i < pieceCount; i++)
        {
            Destroy(allyPool.GetChild(i).gameObject);
        }
    }

   
}
