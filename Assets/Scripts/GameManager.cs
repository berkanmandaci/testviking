using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private InputManager inputManager;
    [SerializeField] private GameObject inGameUI;
    [SerializeField] private GameObject menuUI;
    [SerializeField] private GameObject inGameAndMenuUI;
    [SerializeField] private GameObject winUI;
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    //Click Button
    public void StartGame()
    {
        inputManager.isStart = true;
        inGameUI.SetActive(true);
        menuUI.SetActive(false);
        inputManager.isMenu = false;
        
    }

    public void FinishGame()
    {
        inputManager.isFinish = true;
        winUI.SetActive(true);
        inputManager.isStart = false;
        inGameUI.SetActive(false);
    }

    public void MainMenu()
    {
        inputManager.isMenu = true;
        menuUI.SetActive(true);
        inGameAndMenuUI.SetActive(true);
        inputManager.isFinish = false;
        winUI.SetActive(false);
        Application.LoadLevel(Application.loadedLevel);
    }
}
