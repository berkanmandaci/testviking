using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using DG.Tweening;

public class EnemyController : MonoBehaviour
{
    [SerializeField] private GameObject arrow;
    [SerializeField] private Animator animator;
    [SerializeField] private float arrowFireRange;
    [SerializeField] private float arrowFireRate;
    [SerializeField] private float arrowFireSpeed;
    
    private GameObject player;
    
    // Start is called before the first frame update
    private void Awake()
    {
        player=GameObject.FindWithTag("Positions");
    }

    void Start()
    {
        DOTween.Init();
        InvokeRepeating("Fire",0f,arrowFireRate);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Fire()
    {
        float dist = Vector3.Distance(player.transform.position, transform.position);
        if (arrowFireRange>dist)
        {
           
            // animator.Play(0,0);
            var InstantiateTarget = new Vector3(transform.position.x, transform.position.y + 1f, transform.position.z);
            
            var newArrow=Instantiate(arrow, InstantiateTarget,(Quaternion.identity));
            var Target = new Vector3(player.transform.position.x, player.transform.position.y + 1f, player.transform.position.z);

            newArrow.transform.DOMove(Target, arrowFireSpeed);
        } 
    }

   
}
